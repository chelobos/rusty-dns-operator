use error_chain::error_chain;
use serde::{Deserialize, Serialize};
use std::fs::File;
use std::io::Read;

error_chain! {
    foreign_links {
        Io(std::io::Error);
        HttpRequest(reqwest::Error);
        Yaml(serde_yaml::Error);
    }
}

#[derive(Debug, Serialize, Deserialize)]
struct Config {
    token: String,
    folder_id: String,
}

fn main() -> Result<()> {
    let mut file = File::open("cfg.yaml")?;
    let mut filecontents = String::new();
    file.read_to_string(&mut filecontents)?;
    let config: Config = serde_yaml::from_str(&filecontents)?;
    println!("Token: {}", config.token);
    let client = reqwest::blocking::Client::new();
    let res = client
        .get("https://dns.api.cloud.yandex.net/dns/v1/zones")
        .bearer_auth(config.token.trim())
        .query(&[("folderId", &config.folder_id)])
        .send();
    println!("{:?}", res);
    println!("{:?}", res?.text());
    Ok(())
}
